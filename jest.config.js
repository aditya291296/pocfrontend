module.exports = {
  testMatch: [
    "<rootDir>/src/tests/*.{js,jsx,mjs}",
    "<rootDir>/src/tests/?(*.)(spec|test).{js,jsx,mjs}",
  ],
  transform: {
    "^.+\\.(js|jsx|mjs)$": "babel-jest",
  },
  moduleNameMapper: {
    "\\.(css|less)$": "<rootDir>/src/App.js",
  },
  transformIgnorePatterns: ["[/\\\\]node_modules[/\\\\].+\\.(js|jsx|mjs)$"]
};
