import React, { useContext, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import { Grid } from "@material-ui/core";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormLabel from "@material-ui/core/FormLabel";
import Radio from "@material-ui/core/Radio";
import RenderList from "./RenderList";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: "35px",
    textAlign: "center",
  },
  tableCellPadding: {
    padding: "10px 15px",
    whiteSpace: "nowrap",
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[2],
    padding: theme.spacing(2, 4, 3),
  },
}));

const PlanDialog = (props) => {
  const classes = useStyles();
  const { setSelectedPlan, selectedPlan } = props;
  const handleOnPlanClick = (event) => {
    event.preventDefault();
    setSelectedPlan(event.target.value);
  };

  const renderBulletPoints = () => {
    return (
      <div className={classes.root}>
        <RenderList
          listItems={[
            "If you have less that 25 users, you could save costs by selecting the basic plan.",
            "If you have more than 25 users or want a separate environment, choose Enterprise.",
          ]}
        />
      </div>
    );
  };

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <FormLabel component="legend">Choose a Plan</FormLabel>
        <FormControlLabel
          value="Basic"
          label="Basic"
          control={
            <Radio
              checked={selectedPlan === "Basic"}
              onChange={handleOnPlanClick}
              value="Basic"
              color="primary"
              name="radio-button-demo"
            />
          }
        />

        <FormControlLabel
          value="Enterprise"
          label="Enterprise"
          control={
            <Radio
              checked={selectedPlan === "Enterprise"}
              onChange={handleOnPlanClick}
              value="Enterprise"
              color="primary"
              name="radio-button-demo"
            />
          }
        />
        {renderBulletPoints()}
      </Paper>
    </div>
  );
};
export default PlanDialog;
