import React, { Component } from "react";
import GoogleLogin from "react-google-login";
import { Redirect } from "react-router-dom";
import axios from "axios";

const GmailLogin = (props) => {
  return (
    <div className="App">
      <div className="row">
        <div style={{ paddingTop: "20px" }} className="col-sm-12">
          <div className="col-sm-4"></div>
          <div className="col-sm-4">
            <GoogleLogin
              clientId="958758199445-3galj0tm0vmumab31vjjoo7at8rtfevn.apps.googleusercontent.com"
              buttonText="Login with Google"
              onSuccess={props.googleResponse}
              onFailure={props.googleResponse}
            ></GoogleLogin>
          </div>
          <div className="col-sm-4"></div>
        </div>
      </div>
    </div>
  );
};

export default GmailLogin;
