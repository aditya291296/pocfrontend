import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { TextField, InputAdornment } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import FormLabel from "@material-ui/core/FormLabel";
import clsx from "clsx";
import Input from "@material-ui/core/Input";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import FormControl from "@material-ui/core/FormControl";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: "35px",
    justifyContent: "center",
    alignItems: "center",
  },
  tableCellPadding: {
    padding: "10px 15px",
    whiteSpace: "nowrap",
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[2],
    padding: theme.spacing(2, 4, 3),
  },
}));

const AddOrganizationName = (props) => {
  const classes = useStyles();
  const { organizationName, onChange } = props;

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <FormLabel component="legend">Add an Organization Name</FormLabel>
        <FormControl
          className={clsx(classes.margin, classes.textField)}
          variant="outlined"
        >
          <TextField
            type="text"
            style={{ "margin-top": "20px" }}
            name="nameOnCard"
            variant="outlined"
            fullWidth
            label="Organization Name"
            defaultValue={organizationName}
            onChange={(event) => onChange(event)}
          />
        </FormControl>
      </Paper>
    </div>
  );
};

export default AddOrganizationName;
