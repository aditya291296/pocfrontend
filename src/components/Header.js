import React, { useEffect, useState, useContext } from "react";
import Grid from "@material-ui/core/Grid";
import { withRouter } from "react-router-dom";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import { appContext } from "../App";
import clsx from "clsx";
import Popper from "@material-ui/core/Popper";
import MenuList from "@material-ui/core/MenuList";
import Tooltip from "@material-ui/core/Tooltip";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import MenuItem from "@material-ui/core/MenuItem";
import Divider from "@material-ui/core/Divider";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import SidePanel from "./SidePanel";
import Drawer from "@material-ui/core/Drawer";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  hide: {
    display: "none",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: "flex-end",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
  menusBackground: {
    position: "fixed",
    background: "transparent",
    top: "0",
    right: "0",
    bottom: "0",
    left: "0",
    zIndex: "8",
  },
}));

const Header = (props) => {
  const { authUser, setAuthUser } = useContext(appContext);
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = useState(false);
  const [accountMenuOpen, accountMenuSetOpen] = React.useState(false);
  const [ntsMenuOpen, ntsMenuSetOpen] = React.useState(false);
  const [isUserLoggedIn, setIsUserLoggedIn] = useState({});
  const accountAnchorRef = React.useRef(null);

  useEffect(() => {
    if (authUser && authUser.id) {
      setIsUserLoggedIn(true);
    }
  });

  function handleListKeyDown(event) {
    if (event.key === "Tab") {
      event.preventDefault();
      ntsMenuSetOpen(false);
      accountMenuSetOpen(false);
    }
  }

  const accountHandleClose = (event) => {
    if (
      accountAnchorRef.current &&
      accountAnchorRef.current.contains(event.target)
    ) {
      return;
    }
    accountMenuSetOpen(false);
  };

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const renderAppBarHeading = () => {
    return (
      <Grid item>
        <Typography
          variant="h6"
          noWrap
          style={{ cursor: "pointer" }}
          onClick={(e) =>
            authUser && authUser.id
              ? props.history.push("/")
              : props.history.push("/dashboard")
          }
        >
          Boomerang Management Portal
        </Typography>
      </Grid>
    );
  };

  const renderDrawerButton = () => {
    return (
      <IconButton
        color="inherit"
        aria-label="open drawer"
        onClick={handleDrawerOpen}
        edge="start"
        className={clsx(classes.menuButton, open && classes.hide)}
      >
        <MenuIcon />
      </IconButton>
    );
  };

  const accountMenuNavigation = (href) => {
    props.history.push(href);
    accountMenuSetOpen(false);
  };

  const handleProfileMenu = () => {
    accountMenuSetOpen((prevOpen) => !prevOpen);
  };

  const logoutUser = () => {
    localStorage.clear();
    window.location = "/";
  };

  const handleOnClosePanel = (event) => {
    event.preventDefault();
    setOpen(false);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const renderSidePanel = () => {
    return (
      <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="left"
        open={open}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.drawerHeader}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "ltr" ? (
              <ChevronLeftIcon />
            ) : (
              <ChevronRightIcon />
            )}
          </IconButton>
        </div>
        <Divider />
        <SidePanel onClose={handleOnClosePanel} />
      </Drawer>
    );
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          {renderDrawerButton()}
          <Grid
            container
            style={{ width: "100%" }}
            alignItems="center"
            justify="space-between"
          >
            {renderAppBarHeading()}
            {isUserLoggedIn && (
              <>
                <Tooltip title="Account">
                  <IconButton
                    color="inherit"
                    aria-label="Account"
                    onClick={handleProfileMenu}
                    ref={accountAnchorRef}
                    aria-controls={
                      accountMenuOpen ? "account-menu-list-grow" : undefined
                    }
                    aria-haspopup="true"
                  >
                    <AccountCircleIcon />
                  </IconButton>
                </Tooltip>
                <Popper
                  open={accountMenuOpen}
                  anchorEl={accountAnchorRef.current}
                  role={undefined}
                  transition
                  disablePortal
                >
                  {({ TransitionProps, placement }) => (
                    <Grow
                      {...TransitionProps}
                      style={{
                        transformOrigin:
                          placement === "bottom"
                            ? "center top"
                            : "center bottom",
                      }}
                    >
                      <Paper style={{ maxWidth: "300px" }}>
                        <ClickAwayListener onClickAway={accountHandleClose}>
                          <MenuList
                            autoFocusItem={accountMenuOpen}
                            id="account-menu-list-grow"
                            onKeyDown={handleListKeyDown}
                            style={{ paddingBottom: "0" }}
                          >
                            <MenuItem
                              onClick={(e) => accountMenuNavigation("/profile")}
                            >
                              Profile
                            </MenuItem>
                            <MenuItem
                              onClick={(e) =>
                                accountMenuNavigation("/changepassword")
                              }
                            >
                              Change Password
                            </MenuItem>
                            <MenuItem
                              onClick={(e) =>
                                accountMenuNavigation("/configuration-settings")
                              }
                            >
                              Configuration Settings
                            </MenuItem>
                            <Divider />
                            <MenuItem onClick={logoutUser}>Logout</MenuItem>
                          </MenuList>
                        </ClickAwayListener>
                      </Paper>
                    </Grow>
                  )}
                </Popper>
              </>
            )}
          </Grid>
        </Toolbar>
      </AppBar>
      {renderSidePanel()}
    </div>
  );
};

export default withRouter(Header);
