import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import List from "@material-ui/core/List";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import Slide from "@material-ui/core/Slide";
import { Grid, TextField, InputAdornment } from "@material-ui/core";
import FormLabel from "@material-ui/core/FormLabel";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import clsx from "clsx";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: "relative",
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
  root: {
    padding: "35px",
    justifyContent: "center",
    alignItems: "center",
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const AddAdminUserDialog = (props) => {
  const classes = useStyles();
  const { open, handleClose, handleOnSubmit, currentState, handleChange } =
    props;

  const renderMobileNumber = () => {
    return (
      <TextField
        type="text"
        name="mobileNumber"
        variant="outlined"
        fullWidth
        label="Mobile Number"
        defaultValue={currentState.mobileNumber}
        onChange={(event) => handleChange(event)}
      />
    );
  };

  const renderEmail = () => {
    return (
      <TextField
        type="text"
        name="email"
        variant="outlined"
        fullWidth
        label="Email"
        defaultValue={currentState.email}
        onChange={(event) => handleChange(event)}
      />
    );
  };

  const renderlastName = () => {
    return (
      <TextField
        type="text"
        name="lastName"
        variant="outlined"
        fullWidth
        label="Last Name"
        defaultValue={currentState.lastName}
        onChange={(event) => handleChange(event)}
      />
    );
  };

  const renderfirstName = () => {
    return (
      <TextField
        type="text"
        name="firstName"
        variant="outlined"
        fullWidth
        label="First Name"
        defaultValue={currentState.firstName}
        onChange={(event) => handleChange(event)}
      />
    );
  };

  return (
    <div>
      <Dialog
        fullWidth
        maxWidth="sm"
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              onClick={handleClose}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              Cancel
            </Typography>
            <Button autoFocus color="inherit" onClick={handleOnSubmit}>
              save
            </Button>
          </Toolbar>
        </AppBar>
        <List>
          <div className={classes.root}>
            <Grid item xs={6}>
              {renderfirstName()}
            </Grid>

            <Grid item xs={6} style={{ "margin-top": "10px" }}>
              {renderlastName()}
            </Grid>

            <Grid item xs={6} style={{ "margin-top": "10px" }}>
              {renderEmail()}
            </Grid>

            <Grid item xs={6} style={{ "margin-top": "10px" }}>
              {renderMobileNumber()}
            </Grid>
            {/* 

            <Grid item xs={6} style={{ "margin-top": "10px" }}>
              {renderAbbreviatedName()}
            </Grid>

            <Grid
              item
              xs={6}
              style={{
                "margin-top": "10px",
                padding: "15px",
                textAlign: "center",
              }}
            >
              {renderPlanOptions()}
            </Grid> */}
            {/* <Grid item xs={6} style={{ "margin-top": "10px" }}>
              {renderDomainName()}
            </Grid>

            <Grid item xs={6} style={{ "margin-top": "10px" }}>
              {renderCountryNames()}
            </Grid>

            <Grid item xs={6} style={{ "margin-top": "10px" }}>
              {renderEnv()}
            </Grid> */}
          </div>
        </List>
      </Dialog>
    </div>
  );
};

export default AddAdminUserDialog;
