import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepButton from "@material-ui/core/StepButton";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import PlanDialog from "./PlanDialog";
import PaymentInformation from "./PaymentInformation";
import DomainName from "./DomainName";
import AddOrganizationName from "./AddOrganizationName";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    alignContent: "center",
  },
  button: {
    marginRight: theme.spacing(1),
  },
  completed: {
    display: "inline-block",
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

const StepperSection = (props) => {
  const classes = useStyles();
  const getSteps = () => {
    return ["Choose a Plan", "Choose a Domain", "Add Organization Name"];
  };
  const [activeStep, setActiveStep] = React.useState(0);
  const [completed, setCompleted] = React.useState({});
  const [selectedPlan, setSelectedPlan] = React.useState("Basic");
  const steps = getSteps();
  const paymentInfoInitialState = {
    nameOnCard: "",
    creditCardNumber: "",
    expiryDate: "",
    cvv: "",
  };
  const [paymentInformation, setPaymentInformation] = React.useState(
    paymentInfoInitialState
  );
  const [domainName, setDomainName] = React.useState("");
  const [organizationName, setOrganizationName] = React.useState("");

  const handleOnChange = (event) => {
    event.persist();
    setPaymentInformation((previousValues) => ({
      ...previousValues,
      [event.target.name]: event.target.value,
    }));
  };

  const handleOnDomainNameChange = (event) => {
    event.preventDefault();
    setDomainName(event.target.value);
  };

  const handleOrganizationNameChange = (event) => {
    event.preventDefault();
    setOrganizationName(event.target.value);
  };

  const getStepContent = (step) => {
    switch (step) {
      case 0:
        return (
          <div className={classes.root}>
            <PlanDialog
              selectedPlan={selectedPlan}
              setSelectedPlan={setSelectedPlan}
            />
          </div>
        );
      case 1:
        return (
          <DomainName
            domainName={domainName}
            onChange={handleOnDomainNameChange}
          />
        );
      case 2:
        return (
          <AddOrganizationName
            organizationName={organizationName}
            onChange={handleOrganizationNameChange}
          />
        );

      default:
        return "Unknown step";
    }
  };

  const totalSteps = () => {
    return steps.length;
  };

  const completedSteps = () => {
    return Object.keys(completed).length;
  };

  const isLastStep = () => {
    return activeStep === totalSteps() - 1;
  };

  const allStepsCompleted = () => {
    return completedSteps() === totalSteps();
  };

  const handleNext = () => {
    const newActiveStep =
      isLastStep() && !allStepsCompleted()
        ? // It's the last step, but not all steps have been completed,
          // find the first step that has been completed
          steps.findIndex((step, i) => !(i in completed))
        : activeStep + 1;
    setActiveStep(newActiveStep);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleStep = (step) => () => {
    setActiveStep(step);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    setPaymentInformation(paymentInfoInitialState);
    props.onSubmit(event, {
      domain_name: domainName,
      selected_plan: selectedPlan,
      organization_name: organizationName,
    });
  };

  const handleComplete = () => {
    const newCompleted = completed;
    newCompleted[activeStep] = true;
    setCompleted(newCompleted);
    handleNext();
  };

  const handleReset = () => {
    setActiveStep(0);
    setCompleted({});
  };

  return (
    <div className={classes.root}>
      <Stepper nonLinear activeStep={activeStep}>
        {steps.map((label, index) => (
          <Step key={label}>
            <StepButton
              onClick={handleStep(index)}
              completed={completed[index]}
            >
              {label}
            </StepButton>
          </Step>
        ))}
      </Stepper>
      <div>
        {allStepsCompleted() ? (
          <div>
            <Typography className={classes.instructions}>
              All steps completed - you&apos;re finished
            </Typography>
            <Button onClick={handleReset}>Reset</Button>
          </div>
        ) : (
          <div>
            <Typography className={classes.instructions}>
              {getStepContent(activeStep)}
            </Typography>
            <div>
              <Button
                disabled={activeStep === 0}
                onClick={handleBack}
                className={classes.button}
              >
                Back
              </Button>
              <Button
                variant="contained"
                color="primary"
                onClick={handleNext}
                className={classes.button}
              >
                Next
              </Button>
              {activeStep !== steps.length &&
                (completed[activeStep] ? (
                  <Typography variant="caption" className={classes.completed}>
                    Step {activeStep + 1} already completed
                  </Typography>
                ) : (
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={
                      completedSteps() === totalSteps() - 1
                        ? handleSubmit
                        : handleComplete
                    }
                  >
                    {completedSteps() === totalSteps() - 1
                      ? "Finish"
                      : "Complete Step"}
                  </Button>
                ))}
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default StepperSection;
