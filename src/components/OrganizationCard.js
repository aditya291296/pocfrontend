import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

const OrganizationCard = (props) => {
  const classes = useStyles();
  const { organization } = props;
  const bull = <span className={classes.bullet}>•</span>;

  return (
    <Card className={classes.root} variant="outlined">
      <CardContent>
        <Typography
          className={classes.title}
          color="textSecondary"
          gutterBottom
        >
          {organization.organizationName}
        </Typography>
        <Typography variant="h5" component="h2">
          {organization.organizationConfiguration.plan}
        </Typography>
        <Typography className={classes.pos} color="textSecondary">
          {organization.organizationConfiguration.domainName +
            ".boomerangauth.com"}
        </Typography>
        <Typography className={classes.pos} color="textSecondary">
          {organization.organizationConfiguration.abbreviatedName}
        </Typography>

        <Typography className={classes.pos} color="textSecondary">
          {organization.organizationConfiguration.environment}
        </Typography>

        <Typography className={classes.pos} color="textSecondary">
          {organization.organizationConfiguration.region}
        </Typography>
        <Typography variant="body2" component="p">
          <br />
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small">Edit</Button>
      </CardActions>
    </Card>
  );
};

export default OrganizationCard;
