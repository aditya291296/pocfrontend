import React, { useState, useContext, useEffect } from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import DeleteIcon from "@material-ui/icons/Delete";
import toastr from "toastr";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import { makeStyles } from "@material-ui/core/styles";
import {
  UserTableHead,
  isSelected,
  stableSort,
  getComparator,
  getUserId,
} from "./Utils";
import Paper from "@material-ui/core/Paper";
import {
  getUsersForOrganization,
  createAdminUser,
  deleteAdminUser,
} from "../../Api/Api";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import DeleteModal from "../DeleteModal";
import OrganizationDrawer from "../OrganizationDrawer";
import { withRouter } from "react-router-dom";
import CheckIcon from "@material-ui/icons/Check";
import CloseIcon from "@material-ui/icons/Close";
import Badge from "@material-ui/core/Badge";
import AddAdminUserDialog from "../AdminUser/AddAdminUserDialog";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  paper: {
    width: "96%",
    margin: "35px auto",
    height: " 0px",
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
  appBar: {
    position: "relative",
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
}));

const OrganizationUser = (props) => {
  const classes = useStyles();
  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("name");
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(25);
  const [adminUsers, setAdminUsers] = React.useState([]);
  const [showAddUserDialog, setShowAddUserDialog] = React.useState(false);
  const initialState = {
    email: "",
    mobileNumber: "",
    firstName: "",
    lastName: "",
  };
  const [currentState, setCurrentState] = React.useState(initialState);
  const [toBeDeleted, setToBeDeleted] = React.useState(null);
  const [showDeleteModal, setShowDeleteModal] = React.useState(false);

  useEffect(() => {
    let organizationId = props.match.params.organization_id;
    getUsersForOrganization(organizationId)
      .then((response) => {
        setAdminUsers(response.data);
      })
      .catch((error) => toastr.error(error));
  }, []);

  const handleDeleteIconOnClick = (event, row) => {
    event.preventDefault();
    setToBeDeleted(row);
    setShowDeleteModal(true);
  };

  const renderDeleteIcon = (row) => {
    return (
      <DeleteIcon
        onClick={(event) => handleDeleteIconOnClick(event, row)}
        color="primary"
        style={{
          cursor: "pointer",
        }}
      />
    );
  };

  const isSelected = (name) => selected.indexOf(name) !== -1;

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const renderCheckOrCross = (boolFlag) => {
    if (boolFlag) {
      return (
        <Badge>
          <CheckIcon style={{ color: "#006400" }} />
        </Badge>
      );
    } else {
      return (
        <Badge>
          <CloseIcon style={{ color: "#8B0000" }} />
        </Badge>
      );
    }
  };

  const handleAddAdminUser = (event) => {
    event.preventDefault();
    setShowAddUserDialog(true);
  };

  const saveAdminUserDetails = (event) => {
    event.preventDefault();
    let state = {
      email: currentState.email,
      organization_id: props.match.params.organization_id,
      first_name: currentState.firstName,
      last_name: currentState.lastName,
      mobile_number: currentState.mobileNumber,
    };
    createAdminUser(state)
      .then((response) => {
        if (response && response.status === 200) {
          setCurrentState(initialState);
          setShowAddUserDialog(false);
          setAdminUsers(response.data);
          toastr.success(
            "We have sent you and email with your password and email verification link. Please have a look!"
          );
        }
      })
      .catch((error) => toastr.error(error));
  };

  const closeAddUserDialog = (event) => {
    event.preventDefault();
    setShowAddUserDialog(false);
  };

  const handleOnConfirmDelete = (event) => {
    event.preventDefault();
    deleteAdminUser(toBeDeleted.id, props.match.params.organization_id)
      .then((response) => {
        if (response && response.status === 200) {
          setShowDeleteModal(false);
          setAdminUsers(response.data);
        }
      })
      .catch((error) => toastr.error(error));
  };

  const handleChange = (event) => {
    event.persist();
    setCurrentState((previousValues) => ({
      ...previousValues,
      [event.target.name]: event.target.value,
    }));
  };

  const closeDeleteModal = (event) => {
    event.preventDefault();
    setShowDeleteModal(false);
  };

  return (
    <div className={classes.root}>
      <Paper
        className={classes.paper}
        style={{
          height: (adminUsers.length * 200).toString() + "px",
        }}
      >
        <>
          <div style={{ fontSize: "40px", float: "left" }}>Admin Users</div>
          <AddCircleIcon
            onClick={(event) => handleAddAdminUser(event)}
            color="primary"
            fontSize="large"
            style={{
              float: "right",
              "margin-top": "29px",
              "margin-right": "30px",
              cursor: "pointer",
            }}
          />
        </>
        <TableContainer component={Paper}>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size={"medium"}
            aria-label="enhanced table"
          >
            <UserTableHead
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={adminUsers.length}
            />
            <TableBody>
              {stableSort(adminUsers, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const isItemSelected = isSelected(row.name);
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <TableRow
                      hover
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={row.name}
                      selected={isItemSelected}
                    >
                      <TableCell align="left">{index + 1}</TableCell>
                      <TableCell id={labelId} scope="row" padding="none">
                        {row.id}
                      </TableCell>
                      <TableCell id={labelId} scope="row" padding="none">
                        {row.email}
                      </TableCell>

                      <TableCell id={labelId} scope="row" padding="none">
                        {row.mobileNumber === "" ? "NA" : row.mobileNumber}
                      </TableCell>

                      <TableCell id={labelId} scope="row" padding="none">
                        {renderCheckOrCross(row.isEmailConfirmed)}
                      </TableCell>

                      <TableCell id={labelId} scope="row" padding="none">
                        {renderCheckOrCross(row.areOnboardingDetailsSaved)}
                      </TableCell>

                      <TableCell id={labelId} scope="row" padding="none">
                        {renderDeleteIcon(row)}
                      </TableCell>
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
      <AddAdminUserDialog
        open={showAddUserDialog}
        handleOnSubmit={saveAdminUserDetails}
        handleClose={closeAddUserDialog}
        currentState={currentState}
        handleChange={handleChange}
      />
      <DeleteModal
        primaryText="Delete Admin User ? "
        secondaryText="Are you sure you want to delete this Admin User ?"
        isOpen={showDeleteModal}
        handleClose={closeDeleteModal}
        confirmDelete={handleOnConfirmDelete}
      />
      {/* 
    <OrganizationDrawer
      open={showAddOrganizationNameSlider}
      handleClose={closeOrganizationNameDrawer}
      onSubmitOrganizationDetails={handleOrganizationDetailsSubmit}
    /> */}
    </div>
  );
};

export default OrganizationUser;
