import React, { useState, useContext, useEffect } from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import DeleteIcon from "@material-ui/icons/Delete";
import toastr from "toastr";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import { makeStyles } from "@material-ui/core/styles";
import {
  EnhancedTableHead,
  stableSort,
  getComparator,
  getUserId,
} from "./Utils";
import Paper from "@material-ui/core/Paper";
import {
  saveUserOnboardingDetails,
  getUserDetails,
  saveOrganizationDetail,
  deleteOrganization,
} from "../../Api/Api";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import DeleteModal from "../DeleteModal";
import OrganizationDrawer from "../OrganizationDrawer";
import { withRouter } from "react-router-dom";
import PersonAdd from "@material-ui/icons/PersonAdd";
import { PromptState } from "msal/lib-commonjs/utils/Constants";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  paper: {
    width: "96%",
    margin: "35px auto",
    height: " 0px",
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
  appBar: {
    position: "relative",
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
}));

const OrganizationDashboard = (props) => {
  const classes = useStyles();
  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("name");
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(25);
  const [showDeleteModal, setShowDeleteModal] = React.useState(false);
  const [toBeDeleted, setToBeDeleted] = React.useState(null);
  const [organizationDetails, setOrganizationDetails] = React.useState([]);
  const [showAddOrganizationNameSlider, setShowAddOrganizationNameSlider] =
    React.useState(false);

  useEffect(() => {
    let userId = getUserId();
    if (userId) {
      getUserDetails(userId).then((response) => {
        setOrganizationDetails(response.data.organizations);
      });
    }
  }, []);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const isSelected = (name) => selected.indexOf(name) !== -1;

  const handleDeleteIconOnClick = (event, row) => {
    event.preventDefault();
    setShowDeleteModal(true);
    setToBeDeleted(row);
  };

  const handleAddPersonIconOnClick = (event, row) => {
    event.preventDefault();
    props.history.push(`organization/user/${row.id}`);
  };

  const renderUserIcon = (row) => {
    return (
      <PersonAddIcon
        onClick={(event) => handleAddPersonIconOnClick(event, row)}
        color="primary"
        style={{
          cursor: "pointer",
        }}
      />
    );
  };

  const renderDeleteIcon = (row) => {
    return (
      <DeleteIcon
        onClick={(event) => handleDeleteIconOnClick(event, row)}
        color="primary"
        style={{
          cursor: "pointer",
        }}
      />
    );
  };

  const handleOnConfirmDelete = (event) => {
    event.preventDefault();
    let userId = localStorage.getItem("userId");
    deleteOrganization(
      toBeDeleted.id,
      toBeDeleted.organizationConfiguration.id,
      userId
    )
      .then((response) => {
        if (response && (response.status === 200 || response.status === 204)) {
          setOrganizationDetails(response.data.organizations);
          setToBeDeleted(null);
          setShowDeleteModal(false);
          toastr.success("Deleted Organization Successfully !");
        }
      })
      .catch((error) => toastr.error(error));
  };

  const closeDeleteModal = (event) => {
    event.preventDefault();
    setShowDeleteModal(false);
    setToBeDeleted(null);
  };

  const handleAddOrganization = (event) => {
    event.preventDefault();
    setShowAddOrganizationNameSlider(true);
  };

  const closeOrganizationNameDrawer = (event) => {
    event.preventDefault();
    setShowAddOrganizationNameSlider(false);
  };

  const handleOrganizationDetailsSubmit = (event, requestData) => {
    event.preventDefault();
    let data = {
      organization_name: requestData.organizationName,
      abbreviated_name: requestData.abbreviatedName,
      environment: requestData.environment,
      domain_name: requestData.domainName,
      region: requestData.region.code,
      selected_plan: requestData.selectedPlan,
    };
    data.user_id = getUserId();
    saveOrganizationDetail(data)
      .then((response) => {
        if (response && response.status === 200) {
          setShowAddOrganizationNameSlider(false);
          setOrganizationDetails(response.data.organizations);
          toastr.success("Created new organization successfully!");
        }
      })
      .catch((error) => toastr.error(error));
  };

  return (
    <div className={classes.root}>
      <Paper
        className={classes.paper}
        style={{
          height: (organizationDetails.length * 200).toString() + "px",
        }}
      >
        <>
          <div style={{ fontSize: "40px", float: "left" }}>Organizations</div>
          <AddCircleIcon
            onClick={(event) => handleAddOrganization(event)}
            color="primary"
            fontSize="large"
            style={{
              float: "right",
              "margin-top": "29px",
              "margin-right": "30px",
              cursor: "pointer",
            }}
          />
        </>
        <TableContainer component={Paper}>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size={"medium"}
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={organizationDetails.length}
            />
            <TableBody>
              {stableSort(organizationDetails, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const isItemSelected = isSelected(row.name);
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <TableRow
                      hover
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={row.name}
                      selected={isItemSelected}
                    >
                      <TableCell align="left">{index + 1}</TableCell>
                      <TableCell id={labelId} scope="row" padding="none">
                        {row.organizationName}
                      </TableCell>
                      <TableCell id={labelId} scope="row" padding="none">
                        {row.organizationConfiguration.domainName}
                      </TableCell>

                      <TableCell id={labelId} scope="row" padding="none">
                        {row.organizationConfiguration.plan}
                      </TableCell>

                      <TableCell id={labelId} scope="row" padding="none">
                        {row.organizationConfiguration.numberOfUsers}
                      </TableCell>

                      <TableCell id={labelId} scope="row" padding="none">
                        {row.organizationConfiguration.abbreviatedName}
                      </TableCell>

                      <TableCell id={labelId} scope="row" padding="none">
                        {row.organizationConfiguration.environment}
                      </TableCell>

                      <TableCell id={labelId} scope="row" padding="none">
                        {row.organizationConfiguration.region}
                      </TableCell>

                      <TableCell id={labelId} scope="row" padding="none">
                        {renderDeleteIcon(row)}
                      </TableCell>

                      <TableCell id={labelId} scope="row" padding="none">
                        {renderUserIcon(row)}
                      </TableCell>
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
      <DeleteModal
        primaryText="Delete Organization ? "
        secondaryText="Are you sure you want to delete this Organization ?"
        isOpen={showDeleteModal}
        handleClose={closeDeleteModal}
        confirmDelete={handleOnConfirmDelete}
      />
      <OrganizationDrawer
        open={showAddOrganizationNameSlider}
        handleClose={closeOrganizationNameDrawer}
        onSubmitOrganizationDetails={handleOrganizationDetailsSubmit}
      />
    </div>
  );
};

export default withRouter(OrganizationDashboard);
