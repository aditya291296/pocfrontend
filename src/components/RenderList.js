import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import FiberManualRecordIcon from "@material-ui/icons/FiberManualRecord";
import ListSubheader from "@material-ui/core/ListSubheader";
const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: 1000,
    backgroundColor: theme.palette.background.paper,
  },
}));

const RenderList = (props) => {
  const classes = useStyles();
  const { listItems } = props;
  return (
    <List className={classes.root} subheader={<li />}>
      {listItems.map((item) => (
        <li key={`section-${item}`} className={classes.listSection}>
          <ul className={classes.ul}>
            <ListItem key={`item-${item}-${item}`}>
              <ListItemIcon>
                <FiberManualRecordIcon />
              </ListItemIcon>
              <ListItemText primary={`${item}`} />
            </ListItem>
          </ul>
        </li>
      ))}
    </List>
  );
};

export default RenderList;
