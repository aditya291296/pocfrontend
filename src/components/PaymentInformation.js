import React, { useContext, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import { Grid, TextField } from "@material-ui/core";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormLabel from "@material-ui/core/FormLabel";
import RenderList from "./RenderList";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: "35px",
    justifyContent: "center",
    alignItems: "center",
  },
  tableCellPadding: {
    padding: "10px 15px",
    whiteSpace: "nowrap",
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[2],
    padding: theme.spacing(2, 4, 3),
  },
}));

const PaymentInformation = (props) => {
  const classes = useStyles();
  const { paymentInformation, handleChange, selectedPlan } = props;
  const [showCVV, setShowCVV] = React.useState(false);

  const renderNameOnCardInput = () => {
    return (
      <TextField
        type="text"
        name="nameOnCard"
        variant="outlined"
        fullWidth
        label="Name on Credit Card"
        defaultValue={paymentInformation.nameOnCard}
        onChange={(event) => handleChange(event)}
      />
    );
  };

  const renderCreditCardNumber = () => {
    return (
      <TextField
        type="text"
        name="creditCardNumber"
        variant="outlined"
        fullWidth
        label="Credit Card Number"
        defaultValue={paymentInformation.creditCardNumber}
        onChange={(event) => handleChange(event)}
      />
    );
  };

  const renderExpiryDate = () => {
    return (
      <TextField
        type="text"
        name="expiryDate"
        variant="outlined"
        fullWidth
        label="dd/YYYY"
        defaultValue={paymentInformation.expiryDate}
        onChange={(event) => handleChange(event)}
      />
    );
  };

  const renderCVV = () => {
    return (
      <TextField
        type="text"
        name="cvv"
        variant="outlined"
        fullWidth
        label="CVV"
        defaultValue={paymentInformation.cvv}
        onChange={(event) => handleChange(event)}
        type={"password"}
        id="cvv"
      />
    );
  };

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <FormLabel component="legend">
          Thanks for choosing the {selectedPlan} plan!
        </FormLabel>
        <div className={classes.root}>
          <Grid item xs={6}>
            {renderNameOnCardInput()}
          </Grid>
          <Grid item xs={6} style={{ "margin-top": "10px" }}>
            {renderCreditCardNumber()}
          </Grid>
          <Grid container item xs={12} spacing={1}>
            <React.Fragment>
              <Grid item xs={3} style={{ "margin-top": "10px" }}>
                {renderExpiryDate()}
              </Grid>
              <Grid item xs={3} style={{ "margin-top": "10px" }}>
                {renderCVV()}
              </Grid>
            </React.Fragment>
          </Grid>
        </div>
      </Paper>
    </div>
  );
};
export default PaymentInformation;
