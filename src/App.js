import "regenerator-runtime/runtime";
import "core-js/stable";
import React, { createContext, useEffect, useState } from "react";
import "./App.css";
import {
  createMuiTheme,
  ThemeProvider,
  responsiveFontSizes,
} from "@material-ui/core/styles";
import MainRouter from "./routers/MainRouter";
import { CssBaseline } from "@material-ui/core";

const font1 = "'Lexend', sans-serif";
const font2 = "'DM Serif Display', sans-serif";

export let theme = createMuiTheme({
  palette: {
    primary: {
      main: "#262349", //default orange
    },
    secondary: {
      main: "#FBFBFB", //whitish bg
    },
    tertiary: {
      main: "#14213D", //dark blue - text & bg
    },
    defaultWhite: {
      main: "#FFF",
    },
    defaultBlack: {
      main: "#000",
    },
    text: {
      main: "#707070",
    },
  },
  typography: {
    a: {
      fontFamily: font1,
      fontSize: 14,
    },
    b: {
      fontFamily: font2,
      fontSize: 18,
    },
  },
});

theme = responsiveFontSizes(theme);

export const appContext = createContext();

function App() {
  const [authUser, setAuthUser] = useState(null);

  // useEffect(() => {
  //   var parsedAuthUser;
  //   if (!authUser) {
  //     const localAuthUser = localStorage.getItem("user");
  //     if (localAuthUser) {
  //       parsedAuthUser = JSON.parse(localAuthUser);
  //       setAuthUser(parsedAuthUser);
  //     }
  //   } else {
  //     parsedAuthUser = authUser;
  //   }
  // }, [authUser]);
  return (
    <div>
      <ThemeProvider theme={theme}>
        <appContext.Provider
          value={{
            authUser,
            setAuthUser,
          }}
        >
          <CssBaseline />
          <MainRouter />
        </appContext.Provider>
      </ThemeProvider>
    </div>
  );
}

export default App;
