import React, { useContext } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Header from "../components/Header";
import { appContext } from "../App";
import LoginSection from "../pages/LoginSection";
import RegisterSection from "../pages/RegisterSection";
import Dashboard from "../pages/Dashboard";
import EmailConfirmationMessage from "../pages/EmailConfirmationMessage";
import OrganizationDashboard from "../components/OrganizationDashboard/OrganizationDashboard";
import OrganizationUser from "../components/OrganizationDashboard/OrganizationUser";

const MainRouter = (props) => {
  const { authUser, setAuthUser } = useContext(appContext);
  return (
    <Router>
      <Header />
      <div style={{ minHeight: 64 }} />
      <>
        <Route exact path="/" component={LoginSection} />
        <Route exact path="/register" component={RegisterSection} />
        <Route exact path="/dashboard" component={Dashboard} />
        <Route exact path="/organizations" component={OrganizationDashboard} />
        <Route
          exact
          path="/organization/user/:organization_id"
          component={OrganizationUser}
        />

        <Route
          exact
          path="/email_confirmation_message"
          component={EmailConfirmationMessage}
        />
        {authUser && authUser.id && <Switch></Switch>}
      </>
    </Router>
  );
};

export default MainRouter;
