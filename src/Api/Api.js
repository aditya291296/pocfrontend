import axios from "axios";
import { axiosInstance } from "./setAuthorizationToken";
import { UserControllerClient } from "../user_grpc_web_pb";
import { UserListRequest, User, setPassword, setEmail } from "../user_pb";

// Might be used in the future
// const userClient = new UserControllerClient(
//   "http://localhost:50051",
//   null,
//   null
// );
// if (process.env.NODE_ENV === "development") {
//   // const enableDevTools = window.__GRPCWEB_DEVTOOLS__ || (() = > {});
//   const enableDevTools = window.__GRPCWEB_DEVTOOLS__ || (() => {});

//   // enable debugging grpc calls
//   enableDevTools([userClient]);
// }

// const callback = (after) => {
//   const innercallback = (err, resp) => {
//     if (err) {
//       return null;
//     }
//     after(resp.toObject().message);
//     return resp.toObject();
//   };
//   return innercallback;
// };

// export const listUsers = (after) => {
//   const request = new UserListRequest();
//   return userClient.list(request, {}, callback(after));
// };

// export const createUser = (user, after) => {
//   const request = new User();
//   request.setEmail(user.email);
//   request.setPassword(user.password);
//   return userClient.create(request, {}, callback(after));
// };

const baseURL = "http://localhost:5000";

export const loginUser = (requestData) => {
  return new Promise((resolve, reject) => {
    axios
      .post("v1/admin_user/login", requestData)
      .then((result) => {
        if (result) {
          resolve(result);
          localStorage.setItem("accessToken", result.data.accessToken);
        }
      })
      .catch((error) => reject(error));
  });
};

export const registerUser = (requestData) => {
  return new Promise((resolve, reject) => {
    axiosInstance
      .post(`v1/admin_user/register`, requestData)
      .then((result) => {
        if (result) {
          resolve(result);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export const loginUserViaGmail = (requestData) => {
  return new Promise((resolve, reject) => {
    axiosInstance
      .post(`user/login/gmail/`, requestData)
      .then((result) => {
        if (result) {
          resolve(result);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export const saveUserOnboardingDetails = (requestData) => {
  return new Promise((resolve, reject) => {
    axiosInstance
      .put(`v1/admin_user/onboarding_details/save`, requestData)
      .then((result) => {
        if (result) {
          resolve(result);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export const getUserDetails = (userID) => {
  return new Promise((resolve, reject) => {
    axiosInstance
      .get(`/v1/admin_user/details/${userID}`)
      .then((result) => {
        if (result) {
          resolve(result);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export const saveOrganizationDetail = (requestData) => {
  return new Promise((resolve, reject) => {
    axiosInstance
      .post(`v1/admin_user/organization/add/`, requestData)
      .then((result) => {
        if (result) {
          resolve(result);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export const deleteOrganization = (
  organizationId,
  organizationConfigurationId,
  userId
) => {
  return new Promise((resolve, reject) => {
    axiosInstance
      .delete(
        `v1/admin_user/organization/delete/${organizationConfigurationId}/${userId}/${organizationId}`
      )
      .then((result) => {
        if (result) {
          resolve(result);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export const getUsersForOrganization = (organizationId) => {
  return new Promise((resolve, reject) => {
    axiosInstance
      .get(`v1/admin_user/organization/users/get/${organizationId}`)
      .then((result) => {
        if (result) {
          resolve(result);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export const createAdminUser = (requestData) => {
  return new Promise((resolve, reject) => {
    axiosInstance
      .post(`v1/admin_user/create`, requestData)
      .then((result) => {
        if (result) {
          resolve(result);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export const deleteAdminUser = (userId, organizationId) => {
  return new Promise((resolve, reject) => {
    axiosInstance
      .delete(`v1/admin_user/delete/${userId}/organization/${organizationId}`)
      .then((result) => {
        if (result) {
          resolve(result);
        }
      })
      .catch((error) => {
        reject(error);
      });
  });
};
