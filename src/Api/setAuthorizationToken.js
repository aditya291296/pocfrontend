import Axios from "axios";

Axios.defaults.baseURL = "http://localhost:5000";

Axios.defaults.headers.post["Content-Type"] = "application/json";

export const axiosInstance = Axios.create({});

axiosInstance.interceptors.request.use(
  async (config) => {
    const token = await localStorage.getItem("accessToken");
    if (token) {
      config.headers.Authorization = "Bearer " + token;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);
