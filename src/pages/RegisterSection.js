import React, { useState, useContext, useEffect } from "react";
import toastr from "toastr";
import { withRouter } from "react-router-dom";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import FormControl from "@material-ui/core/FormControl";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import CircularProgress from "@material-ui/core/CircularProgress";
import { appContext } from "../App";
import { registerUser } from "../Api/Api";

const useStyles = makeStyles((theme) => ({
  paper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const RegisterSection = (props) => {
  const classes = useStyles();
  const [emailId, setEmailId] = useState("");
  const { authUser, setAuthUser } = useContext(appContext);
  const [showPassword, setShowPassword] = useState(true);
  const [password, setPassword] = useState("");
  const [user, setUser] = React.useState("");
  const [confirmedEmail, setConfirmedEmail] = React.useState("");
  const [confirmedPassword, setConfirmedPassword] = React.useState("");

  useEffect(() => {
    toastr.options = {
      closeButton: false,
      debug: false,
      newestOnTop: false,
      progressBar: false,
      positionClass: "toast-top-right",
      preventDuplicates: false,
      onclick: null,
      showDuration: "300",
      hideDuration: "1000",
      timeOut: "5000",
      extendedTimeOut: "1000",
      showEasing: "swing",
      hideEasing: "linear",
      showMethod: "fadeIn",
      hideMethod: "fadeOut",
    };
  }, []);

  const handleEmailChange = (event) => {
    event.preventDefault();
    setEmailId(event.target.value);
  };

  const handleEnterClick = (event) => {
    var code = event.keyCode || event.which;
    if (code === 13) {
      submitDetails(event);
    }
  };

  const submitDetails = (event) => {
    event.preventDefault();
    if (
      emailId &&
      password &&
      emailId === confirmedEmail &&
      password === confirmedPassword
    ) {
      const requestData = {
        email: emailId,
        password: password,
      };

      registerUser(requestData)
        .then((response) => {
          if (
            response &&
            (response.status === 201 || response.status === 200)
          ) {
            toastr.success("User has been registered successfully!");
            setEmailId("");
            setShowPassword(false);
            props.history.push("/email_confirmation_message");
          }
        })
        .catch((error) => toastr.error(error));
    } else {
      toastr.error(
        "Email and Confirmed Email or Password or Confirmed Password dont match !"
      );
    }
  };

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const handleConfirmEmailChange = (event) => {
    event.preventDefault();
    setConfirmedEmail(event.target.value);
  };

  const handleConfirmPasswordChange = (event) => {
    event.preventDefault();
    setConfirmedPassword(event.target.value);
  };

  return (
    <Grid style={{ backgroundColor: "#FFFFF", width: "100%" }}>
      <Container
        style={{ backgroundColor: "#FFFFF" }}
        className="mainLogin"
        maxWidth="xs"
      >
        <CssBaseline />
        <div className={classes.paper}>
          <Typography component="h1" variant="h5">
            Register
          </Typography>
          <form className={classes.form} noValidate>
            <TextField
              onChange={handleEmailChange}
              onKeyPress={(e) => handleEnterClick(e)}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="emailId"
              label="Email Address"
              name="emailId"
              autoComplete="emailId"
              autoFocus
            />
            <TextField
              onChange={handleConfirmEmailChange}
              onKeyPress={(e) => handleEnterClick(e)}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="confirmedEmail"
              label="Confirm Email"
              name="confirmedEmail"
              autoComplete="confirmedEmail"
              autoFocus
            />
            <FormControl
              className="full-width border-radius"
              style={{ width: "100%" }}
            >
              <TextField
                onChange={handlePasswordChange}
                onKeyPress={(e) => handleEnterClick(e)}
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type={showPassword ? "password" : "text"}
                id="password"
                autoComplete="current-password"
              />
              <TextField
                onChange={handleConfirmPasswordChange}
                onKeyPress={(e) => handleEnterClick(e)}
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="confirmedPassword"
                label="Confirm Password"
                type={showPassword ? "password" : "text"}
                name="confirmedPassword"
                autoComplete="confirmedPassword"
                autoFocus
              />
              <InputAdornment position="end" className="showpassword">
                <IconButton
                  aria-label="toggle password visibility"
                  style={{
                    top: "18px",
                    position: "absolute",
                    right: "18px",
                  }}
                  onClick={handleClickShowPassword}
                  edge="end"
                >
                  {showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            </FormControl>
            <Button
              type="button"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={submitDetails}
            >
              Register
            </Button>
            <Grid
              item
              xs={6}
              style={{
                fontSize: "15px",
                float: "left",
                margin: " 10px 10px",
              }}
            >
              <Link href="/">Already registered ? Sign in</Link>
            </Grid>
            <Grid container>
              <Grid item xs></Grid>
            </Grid>
          </form>
        </div>
      </Container>
    </Grid>
  );
};

export default withRouter(RegisterSection);
