import React, { useState, useContext, useEffect } from "react";
import toastr from "toastr";
import { withRouter } from "react-router-dom";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";

import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

import { appContext } from "../App";

const EmailConfirmationMessage = (props) => {
  const getEmailIdFromLocalStorage = () => {
    let localEmail = localStorage.getItem("email");
    if (localEmail) {
      let parsedEmail = JSON.parse(localEmail);
      return parsedEmail.email;
    }
  };

  const [email, setEmail] = React.useState(getEmailIdFromLocalStorage());
  return (
    <Typography variant="subtitle1" align="center">
      An email has been sent to {email} with a confirmation code. Please click
      on the link so that we can verify the email.
    </Typography>
  );
};

export default EmailConfirmationMessage;
