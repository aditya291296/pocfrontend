import React, { useState, useContext, useEffect } from "react";
import toastr from "toastr";
import { withRouter } from "react-router-dom";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import FormControl from "@material-ui/core/FormControl";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import CircularProgress from "@material-ui/core/CircularProgress";
import { appContext } from "../App";
import { loginUser, loginUserViaGmail } from "../Api/Api";
import GmailLogin from "../components/GmailLogin";
import AzureLogin from "../components/AzureLogin";

const useStyles = makeStyles((theme) => ({
  paper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const LoginSection = (props) => {
  const classes = useStyles();
  const initialState = {
    emailId: "",
    plainTextPassword: "",
  };
  const { authUser, setAuthUser } = useContext(appContext);
  const [showPassword, setShowPassword] = useState(true);
  const [loginValues, setLoginValues] = useState(initialState);
  const [password, setPassword] = useState(initialState);

  useEffect(() => {
    toastr.options = {
      closeButton: false,
      debug: false,
      newestOnTop: false,
      progressBar: false,
      positionClass: "toast-top-right",
      preventDuplicates: false,
      onclick: null,
      showDuration: "300",
      hideDuration: "1000",
      timeOut: "5000",
      extendedTimeOut: "1000",
      showEasing: "swing",
      hideEasing: "linear",
      showMethod: "fadeIn",
      hideMethod: "fadeOut",
    };
  }, []);

  const handleLoginDetails = (event) => {
    event.persist();
    setLoginValues((previousValues) => ({
      ...previousValues,
      [event.target.name]: event.target.value,
    }));
  };

  const handleEnterClick = (event) => {
    var code = event.keyCode || event.which;
    if (code === 13) {
      submitDetails(event);
    }
  };

  const submitDetails = (event) => {
    event.preventDefault();
    if (loginValues.emailId && password) {
      const requestData = {
        email: loginValues.emailId,
        password: password,
      };
      loginUser(requestData)
        .then((response) => {
          if (response && response.status === 200) {
            setAuthUser(response.data.user);
            localStorage.setItem("userId", response.data.user.id);
            localStorage.setItem("accessToken", response.data.accessToken);
            props.history.push("/dashboard");
          }
        })
        .catch((error) => toastr.error(error));
    }
  };

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const handleAzureResponse = (resp) => {};

  const handleGoogleResponse = (googleResponse) => {
    let resp = googleResponse.profileObj;
    let requestData = {
      email: resp.email,
      family_name: resp.familyName,
      given_name: resp.givenName,
      google_id: resp.googleId,
      image_url: resp.imageUrl,
      name: resp.name,
      password: "",
    };
    loginUserViaGmail(requestData)
      .then((response) => {
        if (response && response.status === 200) {
          setAuthUser(response.data.user);
          localStorage.setItem(
            "id",
            response.data.user.id
            // JSON.stringify({ id: response.data.user.id })
          );
          localStorage.setItem("accessToken", response.data.accessToken);
          props.history.push("/dashboard");
        }
      })
      .catch((error) => toastr.error(error));
  };

  const PrettyPrintJson = (data) => {
    return (
      <div>
        <pre>{JSON.stringify(data, null, 2)}</pre>
      </div>
    );
  };

  return (
    <Grid style={{ backgroundColor: "#FFFFF", width: "100%" }}>
      <Container
        style={{ backgroundColor: "#FFFFF" }}
        className="mainLogin"
        maxWidth="xs"
      >
        <CssBaseline />
        <div className={classes.paper}>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <form className={classes.form} noValidate>
            <TextField
              onChange={handleLoginDetails}
              onKeyPress={(e) => handleEnterClick(e)}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="emailId"
              label="Email Address"
              name="emailId"
              autoComplete="emailId"
              autoFocus
            />
            <FormControl
              className="full-width border-radius"
              style={{ width: "100%" }}
            >
              <TextField
                onChange={handlePasswordChange}
                onKeyPress={(e) => handleEnterClick(e)}
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type={showPassword ? "password" : "text"}
                id="password"
                autoComplete="current-password"
              />
              <InputAdornment position="end" className="showpassword">
                <IconButton
                  aria-label="toggle password visibility"
                  style={{
                    top: "18px",
                    position: "absolute",
                    right: "18px",
                  }}
                  onClick={handleClickShowPassword}
                  edge="end"
                >
                  {showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            </FormControl>
            <Button
              type="button"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={submitDetails}
            >
              Sign In
            </Button>
            <Grid container>
              <Grid
                item
                xs={6}
                style={{
                  fontSize: "15px",
                  float: "left",
                  margin: " 10px 10px",
                }}
              >
                <Link href="/register">Not registered ? Sign up</Link>
              </Grid>
              <Grid item xs={6}>
                <AzureLogin authHandler={handleAzureResponse} />
              </Grid>
            </Grid>
            <Grid container>
              <GmailLogin googleResponse={handleGoogleResponse} />
            </Grid>
          </form>
        </div>
      </Container>
    </Grid>
  );
};

export default withRouter(LoginSection);
