import React, { useState, useContext, useEffect } from "react";
import toastr from "toastr";
import { withRouter } from "react-router-dom";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import FormControl from "@material-ui/core/FormControl";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import CircularProgress from "@material-ui/core/CircularProgress";
import { appContext } from "../App";
import StepperSection from "../components/Stepper";
import {
  saveUserOnboardingDetails,
  getUserDetails,
  saveOrganizationDetail,
  deleteOrganization,
} from "../Api/Api";
import OrganizationCard from "../components/OrganizationCard";
import OrganizationDrawer from "../components/OrganizationDrawer";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import DeleteIcon from "@material-ui/icons/Delete";
import DeleteModal from "../components/DeleteModal";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  paper: {
    width: "96%",
    margin: "35px auto",
    height: " 0px",
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
  appBar: {
    position: "relative",
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
}));

const Dashboard = (props) => {
  const classes = useStyles();
  const { authUser, setAuthUser } = useContext(appContext);
  const [showStepper, setShowStepper] = React.useState(false);
  const [showAddOrganizationNameSlider, setShowAddOrganizationNameSlider] =
    React.useState(false);

  const getUserId = () => {
    return localStorage.getItem("userId");
  };

  const submitDetails = (event, requestData) => {
    event.preventDefault();
    let data = requestData;

    data.user_id = getUserId();
    saveUserOnboardingDetails(data)
      .then((response) => {
        if (response && response.status === 200) {
          toastr.success("Saved used onboarding details successfully !");
          props.history.push("/organizations");
        }
      })
      .catch((error) => toastr.error(error));
  };

  return (
    <div className={classes.root}>
      <Grid item alignItems="center" alignContent="center" xs={12}>
        <StepperSection onSubmit={submitDetails} />
      </Grid>
    </div>
  );
};

export default withRouter(Dashboard);
