/**
 * @fileoverview gRPC-Web generated client stub for user
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');


var google_protobuf_empty_pb = require('google-protobuf/google/protobuf/empty_pb.js')
const proto = {};
proto.user = require('./user_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.user.UserControllerClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.user.UserControllerPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.user.UserListRequest,
 *   !proto.user.User>}
 */
const methodDescriptor_UserController_List = new grpc.web.MethodDescriptor(
  '/user.UserController/List',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.user.UserListRequest,
  proto.user.User,
  /**
   * @param {!proto.user.UserListRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.user.User.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.user.UserListRequest,
 *   !proto.user.User>}
 */
const methodInfo_UserController_List = new grpc.web.AbstractClientBase.MethodInfo(
  proto.user.User,
  /**
   * @param {!proto.user.UserListRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.user.User.deserializeBinary
);


/**
 * @param {!proto.user.UserListRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.user.User>}
 *     The XHR Node Readable Stream
 */
proto.user.UserControllerClient.prototype.list =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/user.UserController/List',
      request,
      metadata || {},
      methodDescriptor_UserController_List);
};


/**
 * @param {!proto.user.UserListRequest} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.user.User>}
 *     The XHR Node Readable Stream
 */
proto.user.UserControllerPromiseClient.prototype.list =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/user.UserController/List',
      request,
      metadata || {},
      methodDescriptor_UserController_List);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.user.User,
 *   !proto.user.User>}
 */
const methodDescriptor_UserController_Create = new grpc.web.MethodDescriptor(
  '/user.UserController/Create',
  grpc.web.MethodType.UNARY,
  proto.user.User,
  proto.user.User,
  /**
   * @param {!proto.user.User} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.user.User.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.user.User,
 *   !proto.user.User>}
 */
const methodInfo_UserController_Create = new grpc.web.AbstractClientBase.MethodInfo(
  proto.user.User,
  /**
   * @param {!proto.user.User} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.user.User.deserializeBinary
);


/**
 * @param {!proto.user.User} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.user.User)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.user.User>|undefined}
 *     The XHR Node Readable Stream
 */
proto.user.UserControllerClient.prototype.create =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/user.UserController/Create',
      request,
      metadata || {},
      methodDescriptor_UserController_Create,
      callback);
};


/**
 * @param {!proto.user.User} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.user.User>}
 *     Promise that resolves to the response
 */
proto.user.UserControllerPromiseClient.prototype.create =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/user.UserController/Create',
      request,
      metadata || {},
      methodDescriptor_UserController_Create);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.user.UserRetrieveRequest,
 *   !proto.user.User>}
 */
const methodDescriptor_UserController_Retrieve = new grpc.web.MethodDescriptor(
  '/user.UserController/Retrieve',
  grpc.web.MethodType.UNARY,
  proto.user.UserRetrieveRequest,
  proto.user.User,
  /**
   * @param {!proto.user.UserRetrieveRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.user.User.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.user.UserRetrieveRequest,
 *   !proto.user.User>}
 */
const methodInfo_UserController_Retrieve = new grpc.web.AbstractClientBase.MethodInfo(
  proto.user.User,
  /**
   * @param {!proto.user.UserRetrieveRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.user.User.deserializeBinary
);


/**
 * @param {!proto.user.UserRetrieveRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.user.User)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.user.User>|undefined}
 *     The XHR Node Readable Stream
 */
proto.user.UserControllerClient.prototype.retrieve =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/user.UserController/Retrieve',
      request,
      metadata || {},
      methodDescriptor_UserController_Retrieve,
      callback);
};


/**
 * @param {!proto.user.UserRetrieveRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.user.User>}
 *     Promise that resolves to the response
 */
proto.user.UserControllerPromiseClient.prototype.retrieve =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/user.UserController/Retrieve',
      request,
      metadata || {},
      methodDescriptor_UserController_Retrieve);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.user.User,
 *   !proto.user.User>}
 */
const methodDescriptor_UserController_Update = new grpc.web.MethodDescriptor(
  '/user.UserController/Update',
  grpc.web.MethodType.UNARY,
  proto.user.User,
  proto.user.User,
  /**
   * @param {!proto.user.User} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.user.User.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.user.User,
 *   !proto.user.User>}
 */
const methodInfo_UserController_Update = new grpc.web.AbstractClientBase.MethodInfo(
  proto.user.User,
  /**
   * @param {!proto.user.User} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.user.User.deserializeBinary
);


/**
 * @param {!proto.user.User} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.user.User)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.user.User>|undefined}
 *     The XHR Node Readable Stream
 */
proto.user.UserControllerClient.prototype.update =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/user.UserController/Update',
      request,
      metadata || {},
      methodDescriptor_UserController_Update,
      callback);
};


/**
 * @param {!proto.user.User} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.user.User>}
 *     Promise that resolves to the response
 */
proto.user.UserControllerPromiseClient.prototype.update =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/user.UserController/Update',
      request,
      metadata || {},
      methodDescriptor_UserController_Update);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.user.User,
 *   !proto.google.protobuf.Empty>}
 */
const methodDescriptor_UserController_Destroy = new grpc.web.MethodDescriptor(
  '/user.UserController/Destroy',
  grpc.web.MethodType.UNARY,
  proto.user.User,
  google_protobuf_empty_pb.Empty,
  /**
   * @param {!proto.user.User} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  google_protobuf_empty_pb.Empty.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.user.User,
 *   !proto.google.protobuf.Empty>}
 */
const methodInfo_UserController_Destroy = new grpc.web.AbstractClientBase.MethodInfo(
  google_protobuf_empty_pb.Empty,
  /**
   * @param {!proto.user.User} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  google_protobuf_empty_pb.Empty.deserializeBinary
);


/**
 * @param {!proto.user.User} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.google.protobuf.Empty)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.google.protobuf.Empty>|undefined}
 *     The XHR Node Readable Stream
 */
proto.user.UserControllerClient.prototype.destroy =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/user.UserController/Destroy',
      request,
      metadata || {},
      methodDescriptor_UserController_Destroy,
      callback);
};


/**
 * @param {!proto.user.User} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.google.protobuf.Empty>}
 *     Promise that resolves to the response
 */
proto.user.UserControllerPromiseClient.prototype.destroy =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/user.UserController/Destroy',
      request,
      metadata || {},
      methodDescriptor_UserController_Destroy);
};


module.exports = proto.user;

