import React from "react";
import { shallow, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import PlanDialog from "../components/PlanDialog";

configure({ adapter: new Adapter() });
describe("<PlanDialog />", () => {
  it("should render PlanDialog component", () => {
    const component = shallow(<PlanDialog />);
    expect(component.getElements()).toMatchSnapshot();
  });
});
