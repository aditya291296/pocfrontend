import React from "react";
import { shallow, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import AddOrganizationName from "../components/AddOrganizationName";


configure({ adapter: new Adapter() });
describe("<AddOrganizationName />", () => {
  it("should render my component", () => {
    const component = shallow(<AddOrganizationName />);

    expect(component.getElements()).toMatchSnapshot();
  });
});
