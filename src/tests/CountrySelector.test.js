import React from "react";
import { shallow, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import CountrySelector from "../components/CountrySelector";

configure({ adapter: new Adapter() });
describe("<CountrySelector />", () => {
  it("should render CountrySelector component", () => {
    const component = shallow(<CountrySelector />);
    expect(component.getElements()).toMatchSnapshot();
  });
});
