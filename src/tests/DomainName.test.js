import React from "react";
import { shallow, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import DomainName from "../components/DomainName";

configure({ adapter: new Adapter() });
describe("<DomainName />", () => {
  it("should render DomainName component", () => {
    const component = shallow(<DomainName />);
    expect(component.getElements()).toMatchSnapshot();
  });
});
