import React from "react";
import { shallow, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import Header from "../components/Header";

configure({ adapter: new Adapter() });
describe("<Header />", () => {
  it("should render Header component", () => {
    const component = shallow(<Header />);
    expect(component.getElements()).toMatchSnapshot();
  });
});
