import React from "react";
import { shallow, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import OrganizationDrawer from "../components/OrganizationDrawer";

configure({ adapter: new Adapter() });
describe("<OrganizationDrawer />", () => {
  it("should render OrganizationDrawer component", () => {
    const component = shallow(<OrganizationDrawer />);
    expect(component.getElements()).toMatchSnapshot();
  });
});
