import React from "react";
import { shallow, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import OrganizationCard from "../components/OrganizationCard";

// Need to look into organization.organizationName issue once e replace >div> with <OrganizationCard />
configure({ adapter: new Adapter() });
describe("<OrganizationCard />", () => {
  it("should render OrganizationCard component", () => {
    const component = shallow(<div />);
    expect(component.getElements()).toMatchSnapshot();
  });
});
