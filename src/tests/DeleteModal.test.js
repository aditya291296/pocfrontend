import React from "react";
import { shallow, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import DeleteModal from "../components/DeleteModal";

configure({ adapter: new Adapter() });
describe("<DeleteModal />", () => {
  it("should render DeleteModal component", () => {
    const component = shallow(<DeleteModal />);
    expect(component.getElements()).toMatchSnapshot();
  });
});
