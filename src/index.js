import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import toastr from "toastr";
import "toastr/build/toastr.css";
import reportWebVitals from "./reportWebVitals";

toastr.options = {
  closeButton: true,
  debug: false,
  newestOnTop: true,
  progressBar: true,
  positionClass: "toast-bottom-full-width",
  preventDuplicates: false,
  onclick: null,
  showDuration: "1000",
  hideDuration: "1000",
  timeOut: "5000",
  extendedTimeOut: "7000",
  showEasing: "swing",
  hideEasing: "linear",
  showMethod: "fadeIn",
  hideMethod: "fadeOut",
};

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
